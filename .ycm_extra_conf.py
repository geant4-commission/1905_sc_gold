# Partially stolen from https://bitbucket.org/mblum/libgp/src/2537ea7329ef/.ycm_extra_conf.py
import os
import ycm_core


# These are the compilation flags that will be used in case there's no
# compilation database set (by default, one is not set).
# CHANGE THIS LIST OF FLAGS. YES, THIS IS THE DROID YOU HAVE BEEN LOOKING FOR.
flags = [
        #'-Wall',
        #'-Wextra',
        #'-Werror',
        '-Wunused',
        '-Wno-long-long',
        '-Wno-variadic-macros',
        '-fexceptions',
        '-DAmoreSim_EXPORTS',
        '-DG4INTY_USE_QT',
        '-DG4INTY_USE_XT',
        '-DG4MULTITHREADED',
        '-DG4UI_USE',
        '-DG4UI_USE_QT',
        '-DG4UI_USE_TCSH',
        '-DG4UI_USE_XM',
        '-DG4VERBOSE',
        '-DG4VIS_USE',
        '-DG4VIS_USE_OI',
        '-DG4VIS_USE_OIX',
        '-DG4VIS_USE_OPENGL',
        '-DG4VIS_USE_OPENGLQT',
        '-DG4VIS_USE_OPENGLX',
        '-DG4VIS_USE_OPENGLXM',
        '-DG4VIS_USE_RAYTRACERX',
        '-DG4_STORE_TRAJECTORY',
        #'-Wunused-arguments',
        # THIS IS IMPORTANT! Without a "-std=<something>" flag, clang won't know which
        # language to use when compiling headers. So it will guess. Badly. So C++
        # headers will be compiled as C headers. You don't want that so ALWAYS specify
        # a "-std=<something>".
        # For a C project, you would set this to something like 'c99' instead of
        # 'c++11'.
        '-DUSE_CLANG_COMPLETER',
        '-std=c++11',
        # ...and the same thing goes for the magic -x option which specifies the
        # language that the files to be compiled are written in. This is mostly
        # relevant for c++ headers.
        # For a C project, you would set this to 'c' instead of 'c++'.
        '-x', 'c++',
        # This path will only work on OS X, but extra paths that don't exist are not
        # harmful
        #'-isystem', '/System/Library/Frameworks/Python.framework/Headers',

        # Place paths from the command
        #  $ echo | gcc -std=c++11 -v -E -x c++ -      when you use gcc
        #  $ echo | clang -std=c++11 -stdlib=libc++ -v -E -x c++ -     when you use clang
        #
        # You can paste below the lines of
        # #include "..." search starts here:
        # #include <...> search starts here:
        # For my laptop
        '-isystem', '/usr/lib/gcc/x86_64-redhat-linux/8/../../../../include/c++/8',
        '-isystem', '/usr/lib/gcc/x86_64-redhat-linux/8/../../../../include/c++/8/x86_64-redhat-linux',
        '-isystem', '/usr/lib/gcc/x86_64-redhat-linux/8/../../../../include/c++/8/backward',
        '-isystem', '/usr/lib/gcc/x86_64-redhat-linux/8/include',
        '-isystem', '/home/basehw/AMoRE_work/include',
        '-isystem', '/storage/software/Geant4/10.04.p02/x86_64/fc28-gcc8.1.1/include/Geant4',
        '-isystem', '/storage/software/root/6.14.02/x86_64/fc28-gcc8.1.1/include',
        '-isystem', '/home/qogksdnr/Geant4_local/CupSoft/AmoreSim_Release/include',
        '-isystem', '/home/qogksdnr/root_local/CUP_work/MyAnlys/AMoRE200/AnlysInfo/include',

        # For CUP cluster
        '-isystem', '/home/basehw/MyUtils/Geant4/9.6-icc/include/Geant4',
        '-isystem', '/home/basehw/MyUtils/root/6.04.14-icc/include',
        '-isystem', '/home/basehw/MyUtils/icc/compilers_and_libraries_2018.2.199/linux/ipp/include',
        '-isystem', '/home/basehw/MyUtils/icc/compilers_and_libraries_2018.2.199/linux/mkl/include',
        '-isystem', '/home/basehw/MyUtils/icc/compilers_and_libraries_2018.2.199/linux/pstl/include',
        '-isystem', '/home/basehw/MyUtils/icc/compilers_and_libraries_2018.2.199/linux/tbb/include',
        '-isystem', '/home/basehw/MyUtils/icc/compilers_and_libraries_2018.2.199/linux/daal/include',
        '-isystem', '/data/MC/AMoRE-pilot/user-scratch/basehw/MyAnlys/AMoRE200/AnlysInfo/include',
        '-isystem', '/home/basehw/AMoRE_work/AmoreSim_Release/include',

        # For common setting
        '-isystem', '/usr/include',
        '-I./include',
        '-I./AMoRE200/AnlysInfo/include'
]

# Set this to the absolute path to the folder (NOT the file!) containing the
# compile_commands.json file to use that instead of 'flags'. See here for
# more details: http://clang.llvm.org/docs/JSONCompilationDatabase.html
#
# Most projects will NOT need to set this to anything; you can just change the
# 'flags' list of compilation flags. Notice that YCM itself uses that approach.
compilation_database_folder = ''

if compilation_database_folder:
    database = ycm_core.CompilationDatabase( compilation_database_folder )
else:
    database = None


def DirectoryOfThisScript():
    return os.path.dirname( os.path.abspath( __file__ ) )


def MakeRelativePathsInFlagsAbsolute( flags, working_directory ):
    if not working_directory:
        return list( flags )
    new_flags = []
    make_next_absolute = False
    path_flags = [ '-isystem', '-I', '-iquote', '--sysroot=' ]
    for flag in flags:
        new_flag = flag

        if make_next_absolute:
            make_next_absolute = False
            if not flag.startswith( '/' ):
                new_flag = os.path.join( working_directory, flag )


            for path_flag in path_flags:
                if flag == path_flag:
                    make_next_absolute = True
                break


            if flag.startswith( path_flag ):
                path = flag[ len( path_flag ): ]
                new_flag = path_flag + os.path.join( working_directory, path )
                break

        if new_flag:
            new_flags.append( new_flag )

    return new_flags




def FlagsForFile( filename ):
    if database:
        # Bear in mind that compilation_info.compiler_flags_ does NOT return a
     # python list, but a "list-like" StringVec object
     compilation_info = database.GetCompilationInfoForFile( filename )
     final_flags = MakeRelativePathsInFlagsAbsolute(
             compilation_info.compiler_flags_,
             compilation_info.compiler_working_dir_ )
    else:
        # relative_to = DirectoryOfThisScript()
        relative_to = os.path.dirname(os.path.abspath(filename))
        final_flags = MakeRelativePathsInFlagsAbsolute( flags, relative_to )


    return {
        'flags': final_flags,
        'do_cache': True
    }

