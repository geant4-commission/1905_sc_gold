//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B4aEventAction.hh
/// \brief Definition of the B4aEventAction class

#ifndef B4aEventAction_h
#define B4aEventAction_h 1

#include <vector>

#include "G4ThreeVector.hh"
#include "G4UserEventAction.hh"
#include "globals.hh"

/// Event action class
///
/// It defines data members to hold the energy deposit and track lengths
/// of charged particles in Absober and Gap layers:
/// - fEnergyAbs, fEnergyGap, fTrackLAbs, fTrackLGap
/// which are collected step by step via the functions
/// - AddAbs(), AddGap()

using namespace std;

class B4aEventAction : public G4UserEventAction {
  public:
    B4aEventAction();
    virtual ~B4aEventAction();

    virtual void BeginOfEventAction(const G4Event *event);
    virtual void EndOfEventAction(const G4Event *event);

    G4int GetEventID() const { return fEventID; }
    vector<G4int> &GetPDGCodeVectorRef() { return fPDGCode; }
    vector<G4double> &GetKineicEnergyVectorRef() { return fKineticEnergy; }
    vector<G4double> &GetPositionXVectorRef() { return fPositionX; }
    vector<G4double> &GetPositionYVectorRef() { return fPositionY; }
    vector<G4double> &GetPositionZVectorRef() { return fPositionZ; }

    void AddOneParticleInfo(const G4int aPDGCode, const G4ThreeVector &aPosition,
                            const G4double aKE) {
        fPDGCode.push_back(aPDGCode);
        fKineticEnergy.push_back(aKE);
        fPositionX.push_back(aPosition.x());
        fPositionY.push_back(aPosition.y());
        fPositionZ.push_back(aPosition.z());
    }

  private:
    G4int fEventID;
    vector<G4int> fPDGCode;
    vector<G4double> fKineticEnergy;
    vector<G4double> fPositionX;
    vector<G4double> fPositionY;
    vector<G4double> fPositionZ;
};

// inline functions

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

