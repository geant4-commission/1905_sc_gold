//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B4DetectorConstruction.cc
/// \brief Implementation of the B4DetectorConstruction class

#include "B4DetectorConstruction.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4Sphere.hh"

#include "G4GeometryManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B4DetectorConstruction::B4DetectorConstruction()
    : G4VUserDetectorConstruction(), fWorldPV(nullptr), fDetectorRegionPV(nullptr),
      fCheckOverlaps(true) {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B4DetectorConstruction::~B4DetectorConstruction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *B4DetectorConstruction::Construct() {
    // Define materials
    DefineMaterials();

    // Define volumes
    return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B4DetectorConstruction::DefineMaterials() {
    // Lead material defined using NIST Manager
    G4NistManager *nistManager = G4NistManager::Instance();
    nistManager->FindOrBuildMaterial("G4_Pb");

    // Liquid argon material
    G4double a; // mass of a mole;
    G4double z; // z=mean number of protons;
    G4double density;
    new G4Material("liquidArgon", z = 18., a = 39.95 * g / mole, density = 1.390 * g / cm3);
    // The argon by NIST Manager is a gas with a different density

    // Vacuum
    new G4Material("Galactic", z = 1., a = 1.01 * g / mole, density = universe_mean_density,
                   kStateGas, 2.73 * kelvin, 3.e-18 * pascal);

    // Print materials
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume *B4DetectorConstruction::DefineVolumes() {
    // Geometry parameters
    G4int nofLayers        = 10;
    G4double absoThickness = 10. * mm;
    G4double calorSizeXY   = 1. * cm;

    // auto calorThickness = nofLayers * layerThickness;
    G4double calorThickness = 10. * um;
    G4double worldSizeXYZ   = 1. * m;

    G4LogicalVolume *detectorRegionMotherLV = nullptr;

    G4double detectorRegionSphereRadius = worldSizeXYZ * 0.99 / 2.;

    G4NistManager *man = G4NistManager::Instance();
    G4Material *gold   = man->FindOrBuildMaterial("G4_Au");
    G4Material *air    = man->FindOrBuildMaterial("G4_AIR");

    //
    // World
    //
    G4Box *worldS = new G4Box("World",                                               // its name
                              worldSizeXYZ / 2, worldSizeXYZ / 2, worldSizeXYZ / 2); // its size

    G4LogicalVolume *worldLV = new G4LogicalVolume(worldS,   // its solid
                                                   air,      // its material
                                                   "World"); // its name

    fWorldPV = new G4PVPlacement(0,               // no rotation
                                 G4ThreeVector(), // at (0,0,0)
                                 worldLV,         // its logical volume
                                 "World",         // its name
                                 0,               // its mother  volume
                                 false,           // no boolean operation
                                 0,               // copy number
                                 fCheckOverlaps); // checking overlaps

    detectorRegionMotherLV = worldLV;

    G4Sphere *detectorRegionSphere = new G4Sphere(
        "DetectorRegion_Sphere", 0, detectorRegionSphereRadius, 0., 360. * deg, 0, 180 * deg);

    G4LogicalVolume *detectorRegionLV =
        new G4LogicalVolume(detectorRegionSphere, worldLV->GetMaterial(), "DetectorRegion_LV");

    G4VisAttributes *detectorRegionVisAttr = new G4VisAttributes({0, 1, 0, 0.4});
    detectorRegionVisAttr->SetForceSolid(true);

    detectorRegionLV->SetVisAttributes(detectorRegionVisAttr);

    fDetectorRegionPV = new G4PVPlacement(nullptr, {}, detectorRegionLV, "DetectorRegion_PV",
                                          detectorRegionMotherLV, false, 0);

    //
    // Calorimeter
    //
    G4Box *calorimeterS =
        new G4Box("Calorimeter",                                         // its name
                  calorSizeXY / 2, calorSizeXY / 2, calorThickness / 2); // its size

    G4LogicalVolume *calorLV = new G4LogicalVolume(calorimeterS,   // its solid
                                                   gold,           // its material
                                                   "Calorimeter"); // its name

    G4RotationMatrix *rm = new G4RotationMatrix();
    rm->rotateX(45. * deg);
    new G4PVPlacement(rm,               // no rotation
                      {},               // at (0,0,0)
                      calorLV,          // its logical volume
                      "Calorimeter",    // its name
                      detectorRegionLV, // its mother  volume
                      false,            // no boolean operation
                      0,                // copy number
                      fCheckOverlaps);  // checking overlaps
    //
    // print parameters
    //
    G4cout << G4endl << "------------------------------------------------------------" << G4endl
           << "---> The calorimeter is " << nofLayers << " layers of: [ " << absoThickness / mm
           << "------------------------------------------------------------" << G4endl;

    //
    // Visualization attributes
    //
    worldLV->SetVisAttributes(G4VisAttributes::GetInvisible());

    G4VisAttributes *simpleBoxVisAtt = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
    simpleBoxVisAtt->SetVisibility(true);
    calorLV->SetVisAttributes(simpleBoxVisAtt);

    //
    // Always return the physical World
    //
    return fWorldPV;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B4DetectorConstruction::ConstructSDandField() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
